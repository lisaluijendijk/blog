

#Onderzoek en HTML
*6-09-2017*

Vanochtend kregen we de opdracht om individueel een thema en een doelgroep te onderzoeken. Ikzelf heb onderzoek gedaan naar eerstejaars studenten aan de Willem de Kooning en kunst in Rotterdam. Later op de dag kregen we een workshop in HTML en heb ik geleerd hoe ik dit moet maken!
Toen we begonnen met onderzoeken hebben we allemaal individueel moodboards gemaakt voor onze zelfgekozen doelgroepen en later hebben we nog een onderzoek gedaan naar verschillende Rotterdamse thema's.
Halverwege de dag kwam een studiecoach ons uitleggen dat we verschillende taken in ons groepje moesten verdelen en nu is mijn taak dus om de planning bij te houden.
Hierna waren de meesten van ons klaar en hebben we gebrainstormd over ons spel, we zijn het er nog niet over eens of het beter is om een bordspel te maken of een spel waarmee je door Rotterdam loopt.

> Written with [StackEdit](https://stackedit.io/).