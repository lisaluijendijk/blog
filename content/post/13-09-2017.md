#Prototyping
_13-09-2017_
Deze dag hebben we ons prototype afgerond en is ons spel speelbaar. Dit prototype moesten we presenteren aan de andere groepjes en na deze presentaties hebben we elkaar feedback gegeven. Wij hebben als feedback gekregen dat we meer Rotterdam moesten verwerken in ons spel en dat we de lay-out van ons bord/spel moesten aanpassen. 
In de middag had ik een workshop over prototyping, dit vond ik zelf erg leuk en we hebben heel veel verschillende ideeën gehad en sommigen hiervan uitgewerkt.
We hebben afgesproken om morgen het spel uit te testen.


> Written with [StackEdit](https://stackedit.io/).